import { CommonModule } from '@angular/common';
import {IonicModule } from '@ionic/angular';
import { TopoLoginComponent } from './topo-login/topo-login.component';
import { NgModule } from '@angular/core';

@NgModule({
    declarations:[
        TopoLoginComponent
    ],
    imports:[
        CommonModule,
        IonicModule
    ],
    exports:[
        TopoLoginComponent
    ]
})
export class ComponentModule{}