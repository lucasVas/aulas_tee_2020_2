// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCBuuj4VzmfMv6jtfrrxC38ci_gJiytOEM',
    authDomain: 'controle-ifv2.firebaseapp.com',
    projectId: 'controle-ifv2',
    storageBucket: 'controle-ifv2.appspot.com',
    messagingSenderId: '664870515270',
    appId: '1:664870515270:web:ca2364cdda4d5a1cda9047',
    measurementId: 'G-CJRSW7N8D4'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
